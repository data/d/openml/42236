# OpenML dataset: datapm2.5

https://www.openml.org/d/42236

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The proposed forecasting approach is tested by using the database from UCI machine learning repository. Using a Deep Learning Model Based on 1D Convnets and Bidirectional GRU

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42236) of an [OpenML dataset](https://www.openml.org/d/42236). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42236/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42236/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42236/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

